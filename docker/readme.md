# Docker
- https://docs.docker.com/guides/get-started/
- https://docs.docker.com/guides/walkthroughs/run-a-container/

We'll use docker, to describe and build a container image, for our
application to run inside. That way, our application runs in a
virtual-environment/OS, and has access to all dependencies it
needs. With this approach we can define images that every
collaborators of a project can use to run the application,
independently on what env/OS they are using. The same way, it can
become practical to run the application in CI/CD and deploy it on a
server.

The requirements are to have docker installed on the machine trying to
run/build a docker image/container.

# What is a container
> A container is an isolated environment for your code. This means
> that a container has no knowledge of your operating system, or your
> files. It runs on the environment it is provided by Docker.
> Containers have everything that your code needs in order to run,
> down to a base operating system. You can use Docker Desktop to
> manage and explore your containers, or the CLI tools

## Ex
- try out the docker desktop GUI application
- try out docker CLI
- explore the different docker commands (run, stop)

## Lifecycle
The container and images from creation to cleanup.
- start `docker` (GUI desktop app, or CLI unix service)
- create/use a `Dockerfile` to describe a container image for an application/project
- create a docker image (with a tag/name), using the `Dockerfile`
- run a docker image (by its tag/name), it creates container (instance of the image)
- debug/monitor/kill running/bugging docker image(s) (our application is running inside)
- groom known registered images and containers (`docker` is a running
  process)

# Run a container
Let run the example docker container using shell commands. For example, if we were to clone a project we want to contribute to, we could follow these steps (which should also be in the project's readme file).
```bash
# clone the example project
git clone https://github.com/docker/welcome-to-docker

# cd /path/to/welcome-to-docker/
docker build -t welcome-to-docker .
```
- `-t` flag tags the image with the name `welcome-to-docker`
- `.` argument lets Docker know where it can find the `Dockerfile`

To run the image we just build, we can run the command:

```bash
docker run -p 3002:3000 welcome-to-docker
```

- the `-p` parameter, to publish the port of the container, to the
host system's interfaces (so we can access the application that is
served via our web browser). Here the port 3000 inside the container,
is exposed to the port 3002 on our local machine (so accessible in the
browser).
- the first argument, `welcome-to-docker` is the name/tag of the
  docker image we're build

# Manage containers
With the GUI, or a CLI, it is possible, and necessary, to manage
registered/running/… container images.

Some usefull commands:
```bash
# access the full documentations
docker help
man docker

# list containers (and their status)
docker ps

# list images
docker images

# remove one or more containers
docker rm

# Remove one or more images
docker rmi
```
# `Dockerfile` to define a containerized application
> A `Dockerfile` is text document that contains all the commands a
> user could call on the command line to assemble an image

- https://docs.docker.com/engine/reference/builder
- alternatively use `docker init` command; see
  https://docs.docker.com/engine/reference/commandline/init
- https://docs.docker.com/develop/develop-images/dockerfile_best-practices/

To define a new docker image, to containerized one of our application,
the requirement is to create a `Dockerfile` in a project's folder. See
the [example in this folder](./Dockerfile).

```bash
docker build -t workshop-test-image .
docker run workshop-test-image
```

> There should be two outputs, one run during build, one run as the
> command

The `FROM` clause is a mandatory instruction in a Dockerfile, it
specifies the base image of the container, here `alpine` linux.

> All changes made to a Dockerfile require to re-build the image if we
> want to get the updated result when we run it as a container

# Docker file environment variables
- https://docs.docker.com/engine/reference/builder/#environment-replacement

> Environment variables are notated in the `Dockerfile` either with
> `$variable_name` or `${variable_name}`. They are treated
> equivalently and the brace syntax is typically used to address
> issues with variable names with no whitespace, like `${foo}_bar`.
