The server is a tiny node.js script using the built-in `http` server
module, reading a data.json file, and answering with its content for
every request. It is accessible on port `3333` when it is run.

# Run with `node.js` on the user machine
Use `npm run dev` to run the server (could imagine it to be a
production, or development servers).

- http://localhost:3333 should show the server response

# Run inside a `docker` container, defined from an image
The `Dockerfile` defines a docker image that starts the server, like
we would do on our machine (example above), but in a Docker container.

```
docker build -t workshop-backend .
docker run workshop-backend -p 2222:3333
```
- http://localhost:2222 should show the server response

# Run with docker, using docker-compose
See readme in the folder above (this lesson).
