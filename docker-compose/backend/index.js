import { createServer } from "http";
import { readFile } from "fs/promises";

const server = createServer(async (req, res) => {
	// Allow requests from localhost:1111
	// NOTE: is for CORS, PORT is out or in of container?
	res.setHeader("Access-Control-Allow-Origin", "http://localhost:3000");
	res.setHeader(
		"Access-Control-Allow-Methods",
		"GET,HEAD,PUT,PATCH,POST,DELETE",
	);
	res.setHeader("Access-Control-Allow-Headers", "Content-Type");
	res.setHeader("Access-Control-Allow-Credentials", "true");

	// Set the content type to JSON
	res.setHeader("Content-Type", "application/json");

	try {
		// Read the content of data.json and send it as the response
		const data = await readFile("data.json", "utf8");
		res.end(data);
	} catch (err) {
		console.error(err);
		res.statusCode = 500;
		res.end("Internal Server Error");
	}
});

// this port is for the backend inside the container
const PORT = 4000;

server.listen(PORT, () => {
	console.log(`Server is listening on port ${PORT}`);
});
