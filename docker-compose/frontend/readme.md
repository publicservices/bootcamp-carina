A frontend client (vanilla javascript web component), that fetches an
endpoint (JSON array), and display it.


# Run in node/npm ecosystem
To server on the port 1111 (by default it is 3000).
```
npx serve -p 1111 .
```

# Run using a Dockerfile
```bash
docker build -t workshop-frontend .
docker run -p 1111:3000 workshop-frontend
```

# Using Docker Compose
See the readme file in the parent folder (this lesson).
