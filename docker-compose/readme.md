# Docker compose
Define a Docker Compose file to configure your Docker application’s
services, networks, volumes, and orchestrate multiple images and
containers.

- https://docs.docker.com/compose/features-uses/
- https://docs.docker.com/compose/compose-file/
- https://docs.docker.com/compose/production/ (compose is traditionaly
  used in dev/test environments, but can be used in production)
- https://docs.docker.com/compose/multiple-compose-files/ (can also
  use multiple compose files together)
- https://docs.docker.com/compose/production/#running-compose-on-a-single-server
  (can use compose to deploy)

We can use docker compose, like docker, to describe and run the
environment of our applications (so they are reproductible), and also
to "orchestrate" how the different containers encapsulating the logics
of our applications (frontend, backend api, database, admin view,
proxy, vpn etc. however our applciation is composed).

## Installation
To use the "compose plugin" for docker, we first need to install it,
so its commands are available to our local environment.
- https://docs.docker.com/compose/install

## Example application
In the current folder, are two applications, a backend and a frontend
are setup to "work together".
- backend, is a web server (vanilla node.js) API, returning a JSON array
  of text strings, for each request to its only endpoint
- frontend, is a HTML page with a javascript code (vanilla
  web-component) fetching the backend's endpoint for its data, and
  displays its content, or an error if the backend is not reachable

### Objectives
Setup this project (the website and the api) as a docker and
docker-compose project, so it could later be put easily online with
CI/CD (in production, for a third party user to access them from their
public IP address or domain name), so other contributors can git clone
our project's repository, and have a way for cooperating on an entire
application that is already well setup

### Challenges
Each the backend and frontend, are Operating System processes
(programs running, until they are stoped), which are both using the
OS's network.

For example the API (running in a docker container or not), is maybe
using the network port 1234 to accept incoming fetch requests.

The same way, the frontend website accessed by a web browser, is
reachable becaused served by a web-server. For this it might use
(inside the container, or normally on our OS), the network port 5678.

If both apps are run on our OS (the "host"), we can access them in our
OS browser by their ports above (`localhost:<port>`)

If we run them both inside their respective container, they cannot be
accessed by their port in our browser's OS, because these ports are
described for "inside the container"; they are parts of each the
container's network.

Both our applications are being run by docker compose, but each of
them are separate docker images/containers, which don't know of each
other. Therefore the frontend cannot fetch the API endpoint, if we do
not "expose the application to the outside world".

The current setup of the (docker-compose + docker) application, is so:
- the frontend and backend are accessible in our OS browsers
- the frontend can fetch the backend and display the response

## Each application can have a `Dockerfile`

The `Dockerfile` in each project's "application folder" (here frontend
and backend), describe their respective required setup.

## One `compose.yaml` file
Inside this folder's `compose.yaml` file, are described two
`services`.  Each of the
[services](https://docs.docker.com/compose/compose-file/05-services/)
are the applications we want to run, and together makes our project
work as we want, where each of their:
- `build` key, to describe in which folder to find the `Dockerfile`
  to [build the image to run a
  container](https://docs.docker.com/compose/compose-file/build/)
- `ports` key, to describe the port mapping from our OS to inside the
  container such as `<OS_PORT>:<CONTAINER_PORT>`

## Workflow
To use `compose` we can run `docker-compose <command>` or `docker
compose <command>`, from inside our project's root folder. A workflow to get started with our apps could be:
```bash
# build all images of all services
docker compose build

# run all images as containers
# can use -d for "detached mode" (as background process)
docker compose up

# stop all containers (compose service)
docker compose down
```

Each time we'll be making changes to one of our app's code (including
the Dockerfile), we'll need to put the containers `down`, `build` the,
and put them `up` again, for our changes to be taken into account.

### Future topics
A list of other things docker-compose can offer, and that we're not
looking at here.

### compose `watch` command
Our example app, is not setup to support the `docker compose watch`
command, which could automatically rebuild images and containers for
every changes in the code and configuration of our compose services.
- https://docs.docker.com/compose/file-watch/

### Networks
Out example app, is not setup to use docker networks (except the
default network).
- https://docs.docker.com/compose/networking/

### Modularity
Our app is a small example, when projects are bigger it is possible to
 modularize the compose files in different ways.
 -https://docs.docker.com/compose/compose-file/10-fragments/
- https://docs.docker.com/compose/compose-file/11-extension/pe
