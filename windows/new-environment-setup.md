# Show the "file" extension and hidden items in file explorer
So we can see and edit the file extensions of all files, and see all
files and folders when browsing files.
- https://support.microsoft.com/en-us/windows/view-hidden-files-and-folders-in-windows-97fbc472-c603-9d90-91d0-1166d1d9f4b5

# Install WSL
We should install `wsl` (Windows Subsystem for Linux) to have a
`unbuntu` linux machine at our disposal.

> It is practical to run a linux virtual machine in our windows
> computer, so our user development environment can use all the tools
> available for any sort of tasks.

- (use the terminal) https://learn.microsoft.com/en-us/windows/terminal
- to install wsl https://learn.microsoft.com/en-us/windows/wsl/install
