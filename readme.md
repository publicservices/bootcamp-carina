# A bootcamp to get familiar with IT for the Digital Humanities
This compilation of documents try to layout introductions to topics
that can help get familiar with Information Technology.

It should help with using computing terminals, for personal discovery
and contributing to digital humanities, covering topics such as system
administration, software and web development, and should be accessible
to all.

See the [matrix space](https://matrix.to/#/#itbdh:matrix.org) for
chats and support.

## Get started
Follow the different topics in this repository, in order or by
interests.

## Operating System(s) and terminals
- get a device, computer, tablet, mobile terminal etc.
- install an Operating System (Linux, osX, Windows etc.), learn more
  about unix
- get familiar and setup the operating system (admin user, disk
  encryption, automatic backups, user settings…)
- tools and communication (create an email address, create a matrix
  account, use a package manager and password manager, discover and
  install git, ssh/gpg…)

## Frontend & shell
- build a website, with or without build tool
- get a "build" output folder of a production website, ready to deploy

### Deployment & CI/CD for client side
- deploy a website/web-app with SASS git connection (cloudflare pages,
  vercele, netlify etc.)
- deploy a static website with custom github actions & gitlab pages
- deploy a website with its build steps in github/gitlab ci/cd

## Databases
- learn about the (postgre)SQL syntax, models, tables, columns
- sqlite3 & sqlite.wasm (for client side)
- postgresql
- alternative databases (google sheet, notion db, )
- (make) database backups, and automate

## Containers
- docker, build a reproductible application image, and run it, inspect
  and explore docker container and logs
- docker-compose, run multiple application images, and make them communicate together

## CLI scripting & Backend (javascript)
- `package.json` for running scripts (in node.js context)
- `make` for running scripts (unix tool)
- build node.js CLI scripts, accept arguments, read ENV vars and config
- build example node.js HTTP server
- discover node.js web-frameworks (express…)
- discover user authentication strategies, libraries, services (auth0,
  magic.link…), JSON web tokens
- build a node.js CRUD backend (user feedback application, with admin view?)


## web/devops and CI/CD of fullstack applications
- deploy a fullstack application on a server, with CI/CD
- implement a backup strategy for the database

## Aditional topics
- security of containers and linux environment
- reporting and application logs
- monitor server & application performances
- alternative containerisation strategies (podman, nixos, guix)
- explore alternative languages (python, golang, rust, bash), and
  their web-frameworks (django etc.)
- discover unix friendly CLI text editors (nano, vim, emacs)
