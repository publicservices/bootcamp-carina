# Install `git` (using `brew`)

If in osX `terminal` app we're typing the `git` command, it will try
to install it outside of `brew`.  This is fine, but we don't know when
this version of git will be updated (or whom in our OS is responsible
for this task of updating git).  If we're interested in having git
updated to its latest version in a way we can manage, we can use brew
to install it.

```bash
# first search for the exact name of the package
brew search git

# then install the package 
brew install git

# if it works the git command should show an output
git
```
