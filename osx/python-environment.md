# python environment and versions

`python` comes installed on a mac operating system, and can be managed by homebrew.

> Homebrew provides formulae to brew Python 3.y. A python@2 formula was provided until the end of 2019, at which point it was removed due to the Python 2 deprecation.

The [homebrew documentation](https://docs.brew.sh/Homebrew-and-Python) has a few recommendations to manage different python versions if we need, including using [pyenv](https://github.com/pyenv/pyenv) (Python Version Management). It also explains more case scenario with `pip` [and other tools from the python ecosytem](https://docs.brew.sh/Homebrew-and-Python#setuptools-pip-etc).
