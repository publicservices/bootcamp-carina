# golang environment and versions

The `go` language development environment, has a built-in update mechanism and version management.
To first install it, we could:

- follow the docs https://go.dev/doc/install
- use brew https://formulae.brew.sh/formula/go
