# Install other software (example of `docker`), and manage them (using `brew`)

Now that we have the `brew` command, we can install programs as packages with it

- https://formulae.brew.sh/formula/docker which tells us `brew install docker` → this installs docker CLI app, not one with a GUI ("Docker Desktop" in our case; to install with brew, see "brew cask")
- https://docs.docker.com/desktop/install/mac-install/ docker can also be installed as a traditional mac application (from a download link to dropping the executable in the OS's "Application" folder). In our example, the "Docker Desktop" app might not give us access the the `docker` CLI command

> When a software is installed with brew, it will behave as usual; brew is only used to install, update and remove the softwares our system need.

> Brew should not be used to manage the "dependencies of a project" (see docker, or project specific package

Generally see the `man bew` page and the [documentation website](https://docs.brew.sh/) for more information on homebrew commands and usages.

To update brew itself, and the packages it manages, we can use the following commands (could be automated as part of our system's maintenance).

```
# update Homebrew itself
brew update

# upgrade all individual packages and formula with the following
brew upgrade
```

Generally, when a software is required for our Operating System administration, we should install it "globally" (on the system) using brew.

Otherwise when a software is used specifically for a project, it should be used in the context of this software using `docker` (or other containerization/virtualization tools).

> If we need to use a software temporarily (for example, to open a database like `sqlite3`, to check data for a project), we should use it from a container image, so the commands we use are reproducible on other OS, their version is specified, and can be documented. We can naturally also install all software globally on our system, using brew or downloading their installer files (for example to try them, or use a specific language environment to script our local system etc.).
