# javascript environment and versions

To be able to run javascript [node.js](https://en.wikipedia.org/wiki/Node.js) scripts, and execute node packages on our operating system, we can use `nvm` the "node version manager". It will help us manage which version of node.js are installed on our machines, and which one is currently used when calling the `node` executable.

- with brew https://formulae.brew.sh/formula/nvm
- or from github https://github.com/nvm-sh/nvm

> See the [usage](https://github.com/nvm-sh/nvm#usage) section of the docs or `nvm --help` for usages

> If we want to explore the javascript "backend/script" alternative ecosystems, we can check [bun.js](https://bun.sh/) and [deno.js](https://deno.com/)
