# brew package manager (and system dependencies)
Install [brew](https://brew.sh/), so we can manage installing and
upgrading the packages & dependencies we need on our machine (like
`apt` on ubuntu linux and the app stores on mobiles).

We will try to use brew to install packages our OS and us need to work
(for example, we will use it to install `git`)

> If we want to install GUI applications with brew too (such as
> firefox, chromium, vlc etc.) we can use "brew cask".

Installing brew should install `xcode` if not already, otherwise run:

```bash
xcode-select --install
```

> Xcode is an integrated development environment for macOS containing
> a suite of software development tools developed by Apple for
> developing software for macOS, iOS, watchOS and tvOS.
