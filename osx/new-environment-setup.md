# New (Apple) mac osX environment setup
How to have a clean new install for having a default working mac OS
operating system enviroment for DevOps work on a computer.

> Knowing this guide is not required, it is more important to
> understand the general logic of keeping a system tidy organized, be
> able to manage and update it, and look all answers online or in the
> manual and help pages of the softwares/environments.

When installing a new apple mac osX machine, we need to make sure that
certain settings optional are correctly setup as we want them

- use a new "Admin user account", to leaving the initial account
  untouched and ensure that we always have a working account to login
  with
- setup an iCloud account to synchronize between devices, and locate a
  lost device etc.
- enable osX "File Vault" full-disk encryption system, so the
  hard-drive is protected against physical access to our computer
- setup a [firmware password to stop recovery mode
  access](https://support.apple.com/en-us/HT201314)
- setup osX "Time Machine" backups for our entire system (on an
  external drive/server)
- setup osX "Finder" app to show the "full unix path" of the files and
  folders, the "user folder shortcut", "show all hardrives", list all
  hidden files etc.
- setup our osX user "Settings", and application settings, such as
  "Spotlight" preferences, to not share data with apple and partners,
  and with our preferences, trackpad clicks etc.
