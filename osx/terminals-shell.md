# Terminal applications to use the shell and CLI softwares

The mac osX `terminal` app allow to use the shell. Some alternative
terminal applications/emulators can be installed, and allow additional
type of feature and customization. Try to find a crossplatform & libre
terminal app

- https://alacritty.org
- https://iterm2.com
- https://hyper.is
- the terminal in `vscode`
- `eshell` and `ansi-term` in emacs

We can also look into the meaning of the different words used in this
context (and all configuration files we can use):

- https://en.wikipedia.org/wiki/Terminal
- https://en.wikipedia.org/wiki/Terminal\_emulator
- https://en.wikipedia.org/wiki/Command-line\_interface
- https://en.wikipedia.org/wiki/Unix\_shell
- https://en.wikipedia.org/wiki/Bash\_(Unix\_shell)

> [tmux](https://en.wikipedia.org/wiki/Tmux) is a CLI terminal
> multiplexer, that can be used to manage multiple terminal, long
> running processes, and even share a terminal session with other. It
> can be used in any unix terminal emulator.

> Enable "bash completions", maybe make them "case insensitive"
