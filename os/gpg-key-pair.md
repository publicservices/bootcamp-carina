# GPG keypair
We can use `gpg` for secure communication and data protection, via encryption, similarly to SSH.

> GPG can be setup and learned when needed, not required to start with, good to know in the long run; and can also be used to manage the SSH keys

- [generate a new GPG key](https://docs.github.com/en/authentication/managing-commit-signature-verification/generating-a-new-gpg-key) private/public pair
- detailed [GPG tutorial and usage on arch linux docs](https://wiki.archlinux.org/title/GnuPG)
- like SSH, the GPG private keys should be protected by a (strong) master password
- discover how to backup our private/public keys, manage the public keys of our contacts, encrypt messages using our private key and the public key of our contacts, decrypt messages sent to us using our private key, use symetric password encryption, key validity management…
- explore gpg in the context of communication (email from/to any emails); also see [protonmail](https://proton.me/), [posteo](https://posteo.de/) mail services which support gpg keys (which they generate for us, and we can export)

In the development and communication contexts, GPG could also used to sign git commits (proving who made them), encrypt notifications and messages sent by git providers (gitlab, github etc.)…
