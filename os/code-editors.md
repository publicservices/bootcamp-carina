# Code editors

To edit code on our local machine, or on remote servers, we can use a code editor or IDE; let's install at least one of them (maybe with `brew` if they are availalbe), and try to see if some are already installed by default (if so, can try their respective built in tutorials, help pages, or check intro material online), and can be either CLI or GUI applications. The idea is to find one that suits what we're feeling comfortable with, and to explore existing ones to see the capabilities they offer.

- [vscode](https://code.visualstudio.com/) (open-source microsoft)
- [nano](https://en.wikipedia.org/wiki/GNU_nano) (free-software)
- [vim](https://github.com/vim/vim) (open-source) and `vi` its parent
- [emacs](https://www.gnu.org/software/emacs/) (free-software)

Each of these editors supports features specific to editing programing environment related files and systems, and can be learned from the ground up to become more at ease with how to use them.

> Investigate how to backup, update and synchronize our editor's configuration and plugins between our different computers (some editors also have mobile/terminal versions); each editor have their own way, and some might fit our workflow more than others.

> It is interesting to learn (at some point) how to use a CLI code editors (not GUI), so for example we can edit files on a machine that does not have a screen (and we're connecting to it maybe via SSH).

> There exist other development environment, like `IntelliJ`; which might be useful to some, or required in some cooperation contexts.
