# SSH key pair

To authenticate into servers, services, and also git providers (github, gitlab etc.), we should use a SSH public/private key instead of user/password authentication.

- [generate a new ssh key](https://docs.github.com/en/authentication/connecting-to-github-with-ssh/generating-a-new-ssh-key-and-adding-it-to-the-ssh-agent) (or use your exsiting one); we now have a "private and public" SSH key pair. The public one can be shared, it is an `id` that identifies our key when used. Use a "master password" to protect your private part of SSH key (requested when creating it, can be edited later).
- be sure it is loaded in the ssh-agent of the local OS (the agent is responsible for prompting us the master password to decrypt the private key when our system is trying to use it)
- add the "public key" to the github interface, in the settings of our user for "SSH/GPG" as a new SSH key
- try the shell command `ssh git@github.com` which should show a success message if our system is correctly setup for git over ssh
