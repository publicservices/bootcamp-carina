# Password management
To [manage all our passwords](https://en.wikipedia.org/wiki/Password_management) and secrets, we can use a "password manager", to keep them in a secure (encrypted) way.
We should install one, and start saving our passwords into it. It is nice to chose one that works "cross platform", so we can have multiple different machines, and maybe have a way to share passwords/secrets with the teams we cooperate with.

- https://keepass.info (GUI, free-software), accessible and practical
- https://www.passwordstore.org (CLI & GUI, free-software), saves in files and folders encrypted in GPG, as a git repository
- https://1password.com (GUI, closed sources, paywall), accessible and well integrated on many platforms

We can/should also use the related "password manager browser plugin", so we can fill-in easily the passwords for the websites we use.

> These passwords managers often have the feature to automatically generate [strong new random passwords](https://xkcd.com/936/) if we want to.
