# Web Browser (and plugins/addons)

Install one (or more, useful for testing in different contexts) modern web browser.

- [firefox](https://www.mozilla.org/firefox/)
- chromium/chrome, brave, nyxt

> Can also explore [CLI web browsers](https://wiki.archlinux.org/title/List_of_applications/Internet#Console) such as `elinks` and `lynx` (can for example browse the web when a graphical environment is not yet installed on a machine, or from an other machine when connected via SSH)

[ublock origin](https://github.com/gorhill/uBlock) ad-blocker, should also be installed to stop getting advertisement from all websites. Install it as a plugin/addon for the browsers we will use.

> Explore how to synchronize the browser's configuration, plugins, user settings, bookmarks etc. between different machines/browser. Maybe let an external "password manager" synchronize passwords, so the most sensitive information is not saved in our browser, but a dedicated program.
